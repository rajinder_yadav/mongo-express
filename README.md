## Mongo Express API and Image server

This **mongo-express** API server is used by the Angular app [Trader Hut](https://bitbucket.org/rajinder_yadav/trader-hut).

### Supported API

**Customer APIs**

Method|URL|Description
------|---|-----------
GET | customer/test | Test, returns string.
GET | customer/:id | Find customer by id value.
GET | customer/page/count | Get page count.
GET | customer/page/:id | Get customer list by page.
POST| customer/ | Create customer.
PUT | customer/ | Update customer.
DELETE| customer/:id | Delete customer.

#### Example for Customer data:

```json
{
  "id": 1,
  "firstName": "Mike",
  "lastName": "McQuaid",
  "gender": "male",
  "address": "1234 Anywhere St.",
  "city": " Phoenix ",
  "state": {
    "abbreviation": "AZ",
    "name": "Arizona"
  },
  "latitude": 33.299,
  "longitude": -111.963
}
```

**Order APIs**

Method|URL|Description
------|---|-----------
GET | order/test | Test, return a string.
GET | order/:id | Fetch orders by customer id.
POST| order/ | Create order for customer.
PUT | order/ | Update order for customer.
DELETE|order/:id| Delete order for customer.

#### Example of Order data.

```json
{
  "id": 1,
  "orders": [
    {"productName": "Basketball", "itemCost": 7.99},
    {"productName": "Shoes", "itemCost": 199.99},
    {"productName": "Socks", "itemCost": 9.01},
    {"productName": "Short", "itemCost": 39.01}
  ]
}
```

### Getting setup

You will need to have Mongo DB installed an running. You can check using:

```sh
mongo --version
```

First run an instance of mongo db (manually). This will create a temp db folder under `/tmp`, so make sure you have write premission to this location (if not modifiy the npm script).

```sh
npm run db:start
```

### Seeing and testing Mongo DB

We must seed the database with some data. This script below will drop old collections and load the db with fresh documents.

```sh
npm run db:seed
```

### Checking Mongo DB data

 Use mongo db shell to check database, we will test both `customers` and `orders` collections.

```sh
mongo
use mongo-express
db.customers.find({id:12})
db.orders.find({id:1})
```

### Testing Express API server

Once the database is setup, we need to check the API server.

Form a new terminal, start the server by typing:

```sh
npm start
```

This will build the TypeScript code and run it.

The server will attempt to connect with Mongo DB first. If it is not running you will see an error, otherwise everything should now be running.

#### Testing Customers router and DB

```sh
# Create new Customer
curl -i \
-H "Content-Type: application/json" \
-X POST -d '{"id":23,"firstName":"Rajinder","lastName":"Yadav","gender":"male","address":"150 Elizabeth St.","city":"Toronto","state":{"abbreviation":"ON","name":"Ontario"},"latitude":43.656572,"longitude":-79.386065}' \
localhost:3000/customer

#  View new Customer
curl -i localhost:3000/customer/23

# Delete Customer we created and verify it does not exist.
curl -X DELETE localhost:3000/customer/23
curl -i localhost:3000/customer/23
```

#### Testing Order router and DB

```sh
# Add a new order
curl -i \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-X POST -d '{"id": 53,"orders": [{"productName": "Boomerang","itemCost": 29.99},{"productName": "Helmet","itemCost": 19.99},{"productName": "Kangaroo Saddle","itemCost": 179.99}]}' \
localhost:3000/order

# View new order
curl -i localhost:3000/order/53

# Modify new order, removed product "Boomerang"
curl -i \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-X PUT -d '{"id": 53,"orders": [{"productName": "Helmet","itemCost": 19.99},{"productName": "Kangaroo Saddle","itemCost": 179.99}]}' \
localhost:3000/order

# View new order
curl -i localhost:3000/order/53

# Delete order we created and make sure it does not exist.
curl -X DELETE localhost:3000/order/53
curl -i localhost:3000/order/53
```
