// import * as bunyan from "bunyan";

import * as express from "express";
import { customerRouter } from "./router/customer.router";
import { orderRouter } from "./router/order.router";
import { dbInit } from "./db/db";
import * as bodyParser from "body-parser";
import * as path from "path";
import * as cors from "cors";

const app = express();
app.use( cors() );
app.use( bodyParser.urlencoded( { extended: true } ) );
app.use( bodyParser.json() );
app.use( "/img", express.static( path.join( __dirname, "images" ) ) );
app.use( "/customer", customerRouter );
app.use( "/order", orderRouter );
app.get( "/", ( req, res ) => {
  res.end( "Home page." );
} );

// Connect to the database, then start express server
dbInit().then( () => app.listen( 3000 ) );

// const log = bunyan.createLogger( {
//   name: "main",
//   streams: [
//     {
//       level: "info",
//       path: "./logs/main.log"
//     }
//   ]
// } );

// log.info( "Entering main" );
