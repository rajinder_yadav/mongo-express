import * as express from "express";
import {
  fetchOrdersByCustomerId,
  createOrder,
  updateOrder,
  deleteOrder
} from "../db/orders.db";

const router = express.Router();

router.get( "/test", ( req, res ) => {
  res.end( "List of orders." );
} );

router.get( "/:id", ( req, res ) => {
  fetchOrdersByCustomerId( parseInt( req.params.id ) )
    .then( docs => res.json( docs ).end() )
    .catch( err => {
      // Log error
      console.log( err );
      res.status( 400 ).end();
    } );
} );

router.post( "/", ( req, res ) => {
  createOrder( req.body )
    .then( r => {
      if ( r.ok === 1 ) {
        res.status( 200 ).end();
      } else {
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

router.put( "/", ( req, res ) => {
  updateOrder( req.body )
    .then( r => {
      if ( r.ok === 1 ) {
        res.status( 200 ).end();
      } else {
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

router.delete( "/:id", ( req, res ) => {
  deleteOrder( parseInt( req.params.id ) )
    .then( r => {
      if ( r.ok === 1 ) {
        res.status( 200 ).end();
      } else {
        // Error, send client BAD request.
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

export const orderRouter = router;
