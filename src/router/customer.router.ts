import * as express from "express";
import {
  fetchCustomerById,
  fetchCustomersByPage,
  createCustomer,
  updateCustomer,
  deleteCustomer,
  pageCount
} from "../db/customers.db";

const router = express.Router();

router.get( "/test", ( req, res ) => {
  res.end( "List of people." );
} );

router.get( "/:id", ( req, res ) => {
  fetchCustomerById( parseInt( req.params.id ) )
    .then( docs => res.json( docs ).end() )
    .catch( err => {
      console.log( err );
      res.status( 400 ).end();
    } );
} );

router.get( "/page/count", ( req, res ) => {
  pageCount().then( count => res.status( 200 ).json( { count: Math.floor( count / 6 ) } ).end() );
} );

router.get( "/page/:id", ( req, res ) => {
  fetchCustomersByPage( parseInt( req.params.id ) )
    .then( docs => res.json( docs ).end() )
    .catch( err => {
      console.log( err );
      res.status( 400 ).end();
    } );
} );

router.post( "/", ( req, res ) => {
  createCustomer( req.body )
    .then( r => {
      if ( r.count === 1 ) {
        res.status( 200 ).end();
      } else {
        // Error, send client BAD request.
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

router.put( "/", ( req, res ) => {
  updateCustomer( req.body )
    .then( r => {
      if ( r.ok === 1 ) {
        res.status( 200 ).end();
      } else {
        // Error, send client BAD request.
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

router.delete( "/:id", ( req, res ) => {
  deleteCustomer( parseInt( req.params.id ) )
    .then( r => {
      if ( r.ok === 1 ) {
        res.status( 200 ).end();
      } else {
        // Error, send client BAD request.
        res.status( 400 ).end();
      }
    } )
    .catch( err => res.status( 400 ).end() );
} );

export const customerRouter = router;
