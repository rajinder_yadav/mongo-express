import { dbInit } from "./db";

let collection;

dbInit().then( db => collection = db.collection( "orders" ) );

export function fetchOrdersByCustomerId( id: number ): Promise<any> {
  return new Promise( ( resolve, reject ) => {
    collection.find( { id: id } ).toArray( ( err, docs ) => {
      if ( err ) {
        // Log error
        reject( new Error( "Orders were not loaded." ) );
        return;
      }
      resolve( docs );
    } );
  } );
}

export function createOrder( order: any ): Promise<any> {
  return collection.insertOne( order );
}

export function updateOrder( order: any ): Promise<any> {
  return collection.findOneAndReplace( { id: order.id }, order );
}

export function deleteOrder( id: number ): Promise<any> {
  return collection.deleteOne( { id: id } );
}
