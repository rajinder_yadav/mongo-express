import { dbInit } from "./db";

let collection;

dbInit().then( db => collection = db.collection( "customers" ) );

export function fetchCustomerById( id: number ): Promise<any[]> {
  return new Promise( ( resolve, reject ) => {
    collection.find( { id: id } ).toArray( ( err, docs ) => {
      if ( err ) {
        // Log error
        reject( new Error( "Customer profile not loaded." ) );
        return;
      }
      resolve( docs );
    } );
  } );
}

export function fetchCustomersByPage( id: number ): Promise<any[]> {
  return new Promise( ( resolve, reject ) => {
    collection.find().skip( id * 6 ).limit( 6 ).toArray( ( err, docs ) => {
      if ( err ) {
        // Log error
        reject( new Error( "Customers page fetch failed." ) );
        return;
      }
      resolve( docs );
    } );
  } );
}

export function createCustomer( customer: any ): Promise<any> {
  return collection.insertOne( customer );
}

export function updateCustomer( customer: any ): Promise<any> {
  return collection.findOneAndReplace( { id: customer.id }, customer );
}

export function deleteCustomer( id: number ): Promise<any> {
  return collection.deleteOne( { id: id } );
}

export function pageCount(): Promise<any> {
  return collection.count();
}
