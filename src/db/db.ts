import { MongoClient } from "mongodb";

const uri = "mongodb://localhost:27017/mongo-express";

export let DB;

export function dbInit(): Promise<any> {
  return new Promise( ( resolve, reject ) => {
    if ( DB ) {
      resolve( DB );
      return;
    }

    MongoClient.connect( uri, ( err, db ) => {
      if ( err ) {
        // Log error
        reject( new Error( "Fail to connect to database mongo-express." ) );
        return;
      }
      DB = db;
      resolve( db );
    } );
  } );
}

export function dbTerm() {
  DB.close();
  DB = undefined;
}
